<?php

use yii\db\Migration;

/**
 * Class m180411_100400_user
 */
class m180411_100400_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180411_100400_user cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180411_100400_user cannot be reverted.\n";

        return false;
    }
    */
}
