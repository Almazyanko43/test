<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>My  Blog!</h1>
    </div>

    <div class="body-content">


        <?php 
            foreach ($posts as $key => $value) {
                ?>
                <div class="post">
                    <div class="title">
                        <?php echo $value->title; ?>
                    </div>
                    <br>
                    <div class="body">
                        <?php echo $value->body; ?>
                    </div>
                </div>
                <?php 
            }
        ?>


    </div>
</div>

<style type="text/css">

.body-content{
    margin-left: auto;
    margin-right: auto;
    width: 500px;
    word-wrap: break-word;

}
.title {
   
    font-size: 27px;
    background-color: #a8d5f5;
    text-align: center;
    border-bottom: 1px solid black;
}
.post {
    border: 1px solid black;
    font-size: 20px;
    background-color: #85da9496;
    margin-top: 30px;
}
</style>
